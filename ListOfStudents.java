package MyArrayListTommy;

import java.util.*;

public class ListOfStudents {
    public static void main(String[] args) {

        String[] studList = {"Antonov", "Shevchenko", "Boiko", "Shevchenko", "Kravchenko", "Savyckyi", "Antonov", "Shevchenko", "Teteriv", "Mamchenko"};
        List<String> studentList = new ArrayList<>();
        Collections.addAll(studentList, studList);

        Set<String> studentListUniq = new HashSet<>();
        Collections.addAll(studentListUniq, studList);

        Map<String, Integer> studentListMapKey = new HashMap<>();
        studentListMapKey.put("Antonov", 7);
        studentListMapKey.put("Boiko", 5);
        studentListMapKey.put("Kravchenko", 10);
        studentListMapKey.put("Mamchenko", 9);
        studentListMapKey.put("Savyckyi", 8);
        studentListMapKey.put("Shevchenko", 10);
        studentListMapKey.put("Teteriv", 7);

        System.out.println("The number of 1st list: " + studentList.size() + "\n" + studentList);
        System.out.println();
        System.out.println("The number of 2d list: " + studentListUniq.size() + "\n" +studentListUniq );
        System.out.println();
        System.out.println("studentListMapKey: " + studentListMapKey);
    }

}

