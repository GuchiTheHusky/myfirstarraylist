package MyArrayListTommy;

import java.util.*;

public class ListOfStudentsTwo {
    public static void main(String[] args) {
        // 1.
        String[] studList = {"Antonov", "Shevchenko", "Boiko", "Shevchenko", "Kravchenko", "Savyckyi", "Antonov", "Shevchenko", "Teteriv", "Mamchenko"};
        List<String> studentList = new ArrayList<>();
        for (String student : studList){
            studentList.add(student);
        }
        System.out.println(print() + "one: " + studentList);
        System.out.println();

        // 2.
        Set<String> studentListUniq = new HashSet<>();
        for (String student : studList){
            studentListUniq.add(student);
        }
        System.out.println(print()+"two: "+"\nsize: "+studentListUniq.size()+"\n"+studentListUniq);
        System.out.println();

        // 3.
        Map<String, Integer> studentListMapKey = new HashMap<>();
        for (String student : studList){
            studentListMapKey.put(student, student.length());
        }
        System.out.println(print()+"three: "+"\n"+studentListMapKey);


    }
    public static String print(){
        return "Example number ";
    }
}
